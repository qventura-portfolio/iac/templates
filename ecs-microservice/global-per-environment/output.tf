output "alb_target_group_arn" {
  value = "${module.ecs.default_alb_target_group}"
}

output "alb_arn" {
  value = "${module.ecs.alb_arn}"
}

output "cluster_arn" {
  value = "${module.ecs.cluster_arn}"
}

output "vpc_id" {
  value = "${module.ecs.vpc_id}"
}

output "autoscaling_group_name" {
  value = "${module.ecs.autoscaling_group_name}"
}
