output "default_alb_target_group" {
  value = "${module.alb.default_alb_target_group}"
}

output "alb_arn" {
  value = "${module.alb.arn}"
}

output "cluster_arn" {
  value = "${aws_ecs_cluster.cluster.arn}"
}

output "vpc_id" {
  value = "${module.network.vpc_id}"
}

output "autoscaling_group_name" {
  value = "${module.ecs_instances.autoscaling_group_name}"
}
