resource "aws_lb_listener_rule" "service_listener_443" {
  listener_arn = "${var.load_balancer_listener_arn}"

  action {
    type             = "forward"
    target_group_arn = "${var.target_group_arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["${var.path_pattern}"]
  }
}
