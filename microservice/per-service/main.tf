variable "service_name" {}

resource "aws_ecr_repository" "ecr_service_repository" {
  name = "${var.service_name}"
}
