output "application_name" {
  value = "${null_resource.labels.triggers.application_name}"
}

output "environment" {
  value = "${null_resource.labels.triggers.environment}"
}

output "full_environment_name" {
  value = "${null_resource.labels.triggers.full_environment_name}"
}

output "name" {
  value = "${null_resource.labels.triggers.name}"
}

output "organization" {
  value = "${null_resource.labels.triggers.organization}"
}

output "tags" {
  value = "${
    merge(
      map(
        "Name", "${null_resource.labels.triggers.name}",
        "Organization", "${null_resource.labels.triggers.organization}",
        "Environment", "${null_resource.labels.triggers.environment}",
        "FullEnvironmentName", "${null_resource.labels.triggers.full_environment_name}",
        "ApplicationName", "${null_resource.labels.triggers.application_name}"
      ), var.tags
    )
  }"
}
