variable "environment" { }
variable "organization" { }
variable "application_name" { }
variable "tags" { type = "map", default = {} }

resource "null_resource" "labels" {
  triggers {
    full_environment_name = "${var.organization}-${var.environment}"
    name = "${var.organization}-${var.environment}-${var.application_name}"
    organization = "${var.organization}"
    environment = "${var.environment}"
    application_name = "${var.application_name}"
  }
}
