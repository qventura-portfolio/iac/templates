variable "aws_region" {}
variable "domain" {}
variable "ecs_aws_ami" { default = "ami-5ac94e3e" }
variable "ecs_availability_zones" { default = [ "ca-central-1a", "ca-central-1b" ]}
variable "ecs_desired_capacity" { default = "1" }
variable "ecs_instance_max_size" { default = "1" }
variable "ecs_instance_min_size" { default = "1" }
variable "ecs_instance_type" { default = "t2.medium" }
variable "ecs_private_subnet_cidrs" { default = ["10.0.50.0/24", "10.0.51.0/24"] }
variable "ecs_public_subnet_cidrs" { default = ["10.0.0.0/24", "10.0.1.0/24"] }
variable "ecs_vpc_cidr" { default = "10.0.0.0/16" }
variable "environment" {}
variable "org_name" {}
variable "s3_lambda_bucket" {}
variable "s3_lambda_key" {}

module "logging" {
  source            = "git::https://git@gitlab.com/newknow/iac/templates/logging.git?ref=0.1.2//global-per-environment"
  aws_region        = "${var.aws_region}"
  environment       = "${var.environment}"
  org_name          = "${var.org_name}"
  s3_lambda_bucket  = "${var.s3_lambda_bucket}"
  s3_lambda_key     = "${var.s3_lambda_key}"
}

module "ecs_microservice" {
  source                    = "git::https://git@gitlab.com/newknow/iac/templates/ecs-microservice.git?ref=0.1.20//global-per-environment"
  domain                    = "${var.domain}"
  environment               = "${var.environment}"
  ecs_vpc_cidr              = "${var.ecs_vpc_cidr}"
  ecs_public_subnet_cidrs   = "${var.ecs_public_subnet_cidrs}"
  ecs_private_subnet_cidrs  = "${var.ecs_private_subnet_cidrs}"
  ecs_availability_zones    = "${var.ecs_availability_zones}"
  ecs_instance_max_size     = "${var.ecs_instance_max_size}"
  ecs_instance_min_size     = "${var.ecs_instance_min_size}"
  ecs_desired_capacity      = "${var.ecs_desired_capacity}"
  ecs_instance_type         = "${var.ecs_instance_type}"
  ecs_aws_ami               = "${var.ecs_aws_ami}"
}
