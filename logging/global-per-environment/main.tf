variable "aws_region" {}
variable "environment" {}
variable "org_name" {}
variable "s3_lambda_bucket" {}
variable "s3_lambda_key" {}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=master"
  namespace  = "${var.org_name}"
  stage      = "${var.environment}"
  name       = "logging"
}

resource "aws_elasticsearch_domain" "elastic_search" {
  domain_name           = "${module.label.id}"
  elasticsearch_version = "6.0"

  cluster_config {
    instance_type = "t2.small.elasticsearch"
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  ebs_options {
    ebs_enabled = "true"
    volume_size = 10
  }

  access_policies = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "arn:aws:es:ca-central-1:920359490632:domain/${module.label.id}/*"
    }
  ]
}
CONFIG


  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = "${module.label.tags}"
}

resource "aws_lambda_function" "logs_to_es_lambda" {
  s3_bucket        = "${var.s3_lambda_bucket}"
  s3_key           = "${var.s3_lambda_key}"
  description      = "Feed CloudWatch Logs to Amazon ES"
  function_name    = "${module.label.id}"
  role             = "${aws_iam_role.lambda_role.arn}"
  handler          = "index.handler"
  runtime          = "nodejs4.3"
  timeout          = 60

  environment {
    variables = {
      ES_ENDPOINT = "${aws_elasticsearch_domain.elastic_search.endpoint}"
    }
  }
}

data "aws_iam_policy_document" "lambda_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "kibana_policy" {
  statement {
    actions = ["es:*"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "lambda_cloudwatch_logs_policy" {
  statement {
    actions = [
      "es:*"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "${module.label.id}"
  assume_role_policy = "${data.aws_iam_policy_document.lambda_role_policy.json}"
}

resource "aws_iam_role_policy" "cloudwatch_logs_policy" {
  name    = "${module.label.id}"
  role    = "${aws_iam_role.lambda_role.id}"
  policy  = "${data.aws_iam_policy_document.lambda_cloudwatch_logs_policy.json}"
}

resource "aws_lambda_permission" "cloudwatch_lambda_permission" {
  statement_id = "${module.label.id}"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.logs_to_es_lambda.arn}"
  principal = "logs.${var.aws_region}.amazonaws.com"
  source_arn = "${aws_cloudwatch_log_group.log_group.arn}"
}

resource "aws_cloudwatch_log_subscription_filter" "logs-subscription" {
  name            = "${module.label.id}"
  depends_on      = ["aws_lambda_permission.cloudwatch_lambda_permission"]

  log_group_name  = "${aws_cloudwatch_log_group.log_group.name}"
  filter_pattern  = "[timestamp=*Z, serviceName, environment, correlatedRequestId, logLevel, message, metadata]"
  destination_arn = "${aws_lambda_function.logs_to_es_lambda.arn}"
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${module.label.id}"
  tags = "${module.label.tags}"
}
