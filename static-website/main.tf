variable "domain_name" {}
variable "environment" {}
variable "organization" {}
variable "application_name" {}

module "labels" {
  source  = "../labels"
  organization = "${var.organization}"
  environment = "${var.environment}"
  application_name = "${var.application_name}"
}

module "route53" {
  source = "./route53"
  domain_name = "${var.domain_name}"
  environment = "${var.environment}"
  cloudfront_domain_name = "${aws_cloudfront_distribution.default.domain_name}"
  cloudfront_hosted_zone_id = "${aws_cloudfront_distribution.default.hosted_zone_id}"
}

resource "aws_s3_bucket" "application" {
  bucket = "${module.labels.name}"
  acl = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  versioning {
    enabled = true
  }

  tags = "${module.labels.tags}"
}

resource "aws_s3_bucket_policy" "application" {
  bucket = "${aws_s3_bucket.application.id}"
  policy =<<POLICY
{
  "Version": "2012-10-17",
  "Id": "Policy1524407876895",
  "Statement": [
    {
      "Sid": "Stmt1524407872598",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "${aws_s3_bucket.application.arn}/*"
    }
  ]
}
POLICY
}


resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
}

resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = "${aws_s3_bucket.application.bucket_domain_name}"
    origin_id   = "${module.labels.name}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path}"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${module.labels.name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  aliases = ["${var.environment}.${var.domain_name}"]

  tags = "${module.labels.tags}"
}
