variable "domain_name" {}
variable "environment" {}
variable "cloudfront_domain_name" {}
variable "cloudfront_hosted_zone_id" {}

data "aws_route53_zone" "default" {
  name = "${var.domain_name}."
}

resource "null_resource" "route_53_record" {
  triggers {
    subdomain = "${var.environment == "production" ? "www" : var.environment}"
  }
}

resource "aws_route53_record" "ipv4" {
  zone_id = "${data.aws_route53_zone.default.zone_id}"
  name = "${null_resource.route_53_record.triggers.subdomain}.${var.domain_name}"
  type = "A"

  alias {
    name = "${var.cloudfront_domain_name}"
    zone_id = "${var.cloudfront_hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "ipv6" {
  zone_id = "${data.aws_route53_zone.default.zone_id}"
  name = "${null_resource.route_53_record.triggers.subdomain}.${var.domain_name}"
  type = "AAAA"

  alias {
    name = "${var.cloudfront_domain_name}"
    zone_id = "${var.cloudfront_hosted_zone_id}"
    evaluate_target_health = false
  }
}
