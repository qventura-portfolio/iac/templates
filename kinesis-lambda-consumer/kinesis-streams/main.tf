variable "stream_names" { type = "list" }
variable "environment" { }

resource "aws_kinesis_stream" "streams" {
  count             = "${length(var.stream_names)}"
  name              = "${element(var.stream_names, count.index)}-${var.environment}"
  shard_count       = 1
  retention_period  = 24

  tags {
    Environment = "${var.environment}"
  }
}

resource "aws_ssm_parameter" "streams" {
  depends_on  = [ "aws_kinesis_stream.streams" ]
  count       = "${length(var.stream_names)}"
  type        = "String"
  value       = "${element(var.stream_names, count.index)}-${var.environment}"
  overwrite   = true
  name        = "/ServiceDiscovery/${var.environment}/KinesisStreams/${element(var.stream_names, count.index)}"
}
