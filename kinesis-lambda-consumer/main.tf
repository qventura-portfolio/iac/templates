variable "aws_region" { default = "ca-central-1" }
variable "input_streams" { type = "list", default = [] }
variable "environment" {}
variable "output_streams" { type = "list", default = [] }
variable "service_name" {}

module "kinesis-inputs" {
  source        = "./kinesis-inputs"
  stream_names  = "${var.input_streams}"
  lambda_arn    = "${aws_lambda_function.lambda.arn}"
  environment   = "${var.environment}"
}

module "kinesis-outputs" {
  source        = "./kinesis-outputs"
  stream_names  = "${var.output_streams}"
  environment   = "${var.environment}"
}

resource "aws_s3_bucket" "source_bucket" {
  bucket = "newknow-lambdas-${var.environment}"
}

resource "aws_s3_bucket_object" "lambda_source" {
  depends_on = ["null_resource.dummy_lambda"]
  bucket = "${aws_s3_bucket.source_bucket.bucket}"
  key = "source.zip"
  source = "initial_empty_lambda.zip"
}

resource "null_resource" "dummy_lambda" {
  provisioner "local-exec" {
    command = "mkdir -p dist/lambda && echo 'exports.lambdaHandler = function(event, context) {}' >> dist/lambda/index.js && zip initial_empty_lambda.zip dist/lambda/index.js"
  }
}

resource "aws_lambda_function" "lambda" {
  function_name = "${var.service_name}-${var.environment}"
  s3_bucket     = "${aws_s3_bucket.source_bucket.bucket}"
  handler       = "dist/index-lambda.lambdaHandler"
  s3_key        = "source.zip"
  runtime       = "nodejs6.10"
  role          = "${aws_iam_role.api_lambda_role.arn}"
  timeout       = 10

  environment {
    variables = {
      NODE_ENV        = "${var.environment}"
      NEW_RELIC_HOME  = "./node_modules/@qventura/base-service/dist"
      NEW_RELIC_LOG   = "stdout"
    }
  }
}

data "aws_iam_policy_document" "api_lambda_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "api_lambda_role" {
  name               = "api-lambda-role-${var.service_name}-${var.environment}"
  assume_role_policy = "${data.aws_iam_policy_document.api_lambda_role_policy.json}"
}

resource "aws_iam_role_policy" "cloudwatch_logs_policy" {
  name    = "api-lambda-role-${var.service_name}-cloudwatch-logs-policy"
  role    = "${aws_iam_role.api_lambda_role.id}"
  policy  = "${data.aws_iam_policy_document.api_lambda_cloudwatch_logs_policy.json}"
}

data "aws_iam_policy_document" "api_lambda_cloudwatch_logs_policy" {
  statement {
    actions = [
      "ssm:GetParameter",
      "kinesis:PutRecord",
      "kinesis:PutRecords",
      "kinesis:GetRecords",
      "kinesis:GetShardIterator",
      "kinesis:DescribeStream",
      "kinesis:ListStreams",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:FilterLogEvents"
    ]

    resources = ["*"]
  }
}
